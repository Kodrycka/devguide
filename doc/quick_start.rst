Quick start
===========


Clone and build the latest code
-------------------------------

Clone the repository (master branch)::

  $ git clone --recursive https://gitlab.com/dalton/dalton.git

In case of LSDalton::

  $ git clone --recursive https://gitlab.com/dalton/lsdalton.git

Build the code::

  $ ./setup [--help]
  $ cd build
  $ make [-j4]

Run the test set::

  $ ctest [-j4]


Want to contribute changes?
---------------------------

Fork either https://gitlab.com/dalton/dalton or https://gitlab.com/dalton/lsdalton,
commit your changes, and file a merge request. Always use feature branches, never push to the master branch.

Developer mailing list
----------------------

All developers should sign up to the dalton-wizards mailing list. Here you get information about 
Dalton meetings, get updates on general disussions, and get information about release re-scheduling and decision 
making outside the Dalton meetings. To sign up contact either Hans Jørgen Jensen (hjj@sdu.dk), 
Trygve Helgaker (t.u.helgaker@kjemi.uio.no) or Kenneth Ruud (kenneth.ruud@uit.no).