

Reporting and fixing bugs
=========================


Where to report bugs
--------------------

We track issues at https://gitlab.com/dalton/dalton/issues.
Please report and discuss bugs there.

In addition you can discuss serious bugs in the developers section of the
`Dalton forum <http://daltonprogram.org/forum>`_.


How to fix bugs
---------------

Previous text was outdated and removed. If you know how to do it, please
document here.
