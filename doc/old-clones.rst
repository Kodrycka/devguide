

I have cloned before August 27 - what now?
==========================================


What happened on August 27?
---------------------------

We have split each of the Dalton and LSDalton repositories into two, a public repository
and a private repository. So instead of two repositories we now have four: two public
(containing the ``master`` and release branches), and two private branches (containing all other branches)::

  https://gitlab.com/dalton/dalton/
  https://gitlab.com/dalton/dalton-private/
  https://gitlab.com/dalton/lsdalton/
  https://gitlab.com/dalton/lsdalton-private/

Few changes may surprise you:

- You will not be able to push to the public repository directly - changes are integrated via merge requests from forks.
- You will not find any ``master`` branch or release branches on the private repository.
- A fork is like a copy of an entire repository but your fork will not automatically update itself, you have to keep it in sync.


If you have uncommitted or unpushed changes
-------------------------------------------

You cannot simply push your changes because 1) you cannot push directly to the
central repository and 2) the branch you want to push to is most probably not
there anymore.

If you have changes on your local ``master`` branch, we recommend to create a feature
branch from the ``master`` branch.

You have now two options to push your feature branches:

To the private repository (the "old" repository)::

  $ git push -u git@gitlab.com:dalton/dalton-private.git my-feature-branch

Or to a fork (adapt "user"). You may need to create the fork first::

  $ git push -u git@gitlab.com:user/dalton.git my-feature-branch

Later you can submit a merge request from your fork. We recommend to push all
modified branches one by one. After you are done, we recommend to create a
fresh clone from your fork. Verify that your changes are there.


If you have no uncommitted or unpushed changes
----------------------------------------------

If you are not sure whether you have uncommitted or unpushed changes on an old
fork, please ask.  If you are sure that you have no uncommitted or unpushed
changes then we recommend to create a fresh clone, either from the central
repository if you do not plan to make changes to the code::

  $ git clone --recursive git@gitlab.com:dalton/dalton.git
  $ git clone --recursive git@gitlab.com:dalton/lsdalton.git

Or if you wish to make changes to the code, first fork the repository and
clone the fork (you need to adapt "user")::

  $ git clone --recursive git@gitlab.com:user/dalton.git
  $ git clone --recursive git@gitlab.com:user/lsdalton.git
